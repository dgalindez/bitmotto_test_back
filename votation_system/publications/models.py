from django.db import models
from django.utils import timezone
from django.contrib.auth.models import User

# Create your models here.
class Publication(models.Model):

    title = models.CharField(max_length=255)
    content = models.TextField()
    author = models.ForeignKey(User, on_delete=models.CASCADE)
    date = models.DateTimeField(default=timezone.now)


class Voting(models.Model):
    publication = models.ForeignKey(Publication, on_delete=models.CASCADE)
    upvotes = models.IntegerField(default=0)
    downvotes = models.IntegerField(default=0)