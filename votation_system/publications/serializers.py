from cgitb import lookup
from rest_framework import serializers
from .models import Publication, Voting

class PublicationSerializer(serializers.ModelSerializer):
    class Meta:
        model = Publication
        fields = ('id', 'title', 'content', 'author', 'date')

class VotingSerializer(serializers.ModelSerializer):
    class Meta:
        model = Voting
        fields = ('id', 'publication', 'upvotes', 'downvotes')