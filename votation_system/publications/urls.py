from django.urls import re_path as url
from . import views

urlpatterns = [
    url(r'^list/$', views.publications, name='publications_list'),
    url(r'^list/(?P<pk>[0-9]+)/$', views.publications_by_id, name='publications_by_id'),
    url(r'^voting/$', views.voting, name='voting_list'),
    url(r'^voting/(?P<id>[0-9]+)/$', views.voting_by_id, name='voting_by_id'),
]