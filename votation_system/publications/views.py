from rest_framework.decorators import api_view
from django.views.decorators.csrf import csrf_exempt
from .models import Publication, Voting
from .serializers import PublicationSerializer, VotingSerializer
from django.http import JsonResponse
from rest_framework.parsers import JSONParser

@csrf_exempt
@api_view(['GET', 'POST'])
def publications(request):
    """
    TODO: comments
    GET and POST methods
    """

    if request.method == 'GET':
        publications = Publication.objects.all()
        serializer = PublicationSerializer(publications, many=True)
        dataAll = serializer.data
        for data in dataAll:
            try:
                data['votings'] = []
                dataVoting = Voting.objects.get(publication_id=data['id'])
                serializer = VotingSerializer(dataVoting)
                aux = serializer.data
                dataOut = {
                    'upvotes': aux['upvotes'],
                    'downvotes': aux['downvotes'],
                }
                data['votings'].append(dataOut)
                dataAll[dataAll.index(data)] = data
            except:
                dataOut = {
                    'upvotes': 0,
                    'downvotes': 0,
                }
                data['votings'].append(dataOut)
                dataAll[dataAll.index(data)] = data
        return JsonResponse(dataAll, status=200, safe=False)

    elif request.method == 'POST':
        data = JSONParser().parse(request)
        serializer = PublicationSerializer(data=data)
        if serializer.is_valid():
            print("Serializador Ok. Se guardara en base de datos.")
            serializer.save()
            return JsonResponse(serializer.data, status=201)

@csrf_exempt
@api_view(['GET', 'PUT', 'DELETE'])
def publications_by_id(request, pk):
    """
    TODO: comments
    GET, UPDATE and DELETE methods
    """

    print("request.method: ", request.method)

    if request.method == 'GET':
        publication = Publication.objects.get(pk=pk)
        serializer = PublicationSerializer(publication)
        dataOutPublication = serializer.data
        try:
            dataOutPublication['votings'] = []
            dataVoting = Voting.objects.get(publication_id=dataOutPublication['id'])
            serializer = VotingSerializer(dataVoting)
            aux = serializer.data
            dataOut = {
                'upvotes': aux['upvotes'],
                'downvotes': aux['downvotes'],
            }
            dataOutPublication['votings'].append(dataOut)

        except:
            dataOut = {
                'upvotes': 0,
                'downvotes': 0,
            }
            dataOutPublication['votings'].append(dataOut)

        return JsonResponse(dataOutPublication, status=200)

    if request.method == 'PUT':
        publication = Publication.objects.get(pk=pk)
        if not publication:
            return JsonResponse({'error': 'Publication not found'}, status=404)
        data = JSONParser().parse(request)
        serializer = PublicationSerializer(publication, data=data)
        if serializer.is_valid():
            serializer.save()
            return JsonResponse(serializer.data, status=200)
        return JsonResponse(serializer.errors, status=400)

    if request.method == 'DELETE':
        publication = Publication.objects.get(pk=pk)
        if not publication:
            return JsonResponse({'error': 'Publication not found'}, status=404)
        publication.delete()
        return JsonResponse({'deleted': True}, status=204)


@csrf_exempt
@api_view(['GET', 'POST'])
def voting(request):
    """
    TODO: comments
    GET and POST methods
    """

    if request.method == 'GET':
        voting = Voting.objects.all()
        serializer = VotingSerializer(voting, many=True)
        data = {
            'data': serializer.data,
        }
        return JsonResponse(data, status=200, safe=False)

    elif request.method == 'POST':
        data = JSONParser().parse(request)
        serializer = VotingSerializer(data=data)
        if serializer.is_valid():
            serializer.save()
            return JsonResponse(serializer.data, status=201)

@csrf_exempt
@api_view(['GET', 'PUT', 'DELETE'])
def voting_by_id(request, id):
    """
    TODO: comments
    GET, UPDATE and DELETE methods
    """

    print("request.method: ", request.method)

    if request.method == 'GET':
        voting = Voting.objects.get(publication_id=id)
        serializer = VotingSerializer(voting)
        aux = serializer.data
        data = {
            'publication':  voting.publication.title,
            'upvotes': aux['upvotes'],
            'downvotes': aux['downvotes'],
        }
        return JsonResponse(data, status=200)

    if request.method == 'PUT':
        try:
            voting = Voting.objects.get(publication_id=id)
            print("voting: ", voting)
            # Si trae data actualizamos
            data = JSONParser().parse(request)
            dataUpdate = {
                'publication': voting.publication_id,
                'upvotes': data['upvotes'] + voting.upvotes,
                'downvotes': data['downvotes'] + voting.downvotes,
            }
            serializer = VotingSerializer(voting, data=dataUpdate)
            if serializer.is_valid():
                serializer.save()
                return JsonResponse(serializer.data, status=205)

            return JsonResponse(serializer.errors, status=400)
        except:
            try:
                print("try")
                data = JSONParser().parse(request)
                serializer = VotingSerializer(data=data)
                if serializer.is_valid():
                    serializer.save()
                    return JsonResponse(serializer.data, status=201)
                print("Error en el serializer: ", serializer.errors)
                return JsonResponse({'error': 'Debe enviar una publicacion valida'}, status=400)
            except:
                return JsonResponse({'error': 'Debe enviar una publicacion valida'}, status=404)

    if request.method == 'DELETE':
        voting = Voting.objects.get(publication_id=id)
        if not voting:
            return JsonResponse({'error': 'Voting not found'}, status=404)
        voting.delete()
        return JsonResponse({'deleted': True}, status=204)